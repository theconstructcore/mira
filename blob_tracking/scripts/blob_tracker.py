#!/usr/bin/env python

"""
ON THE RASPI: roslaunch raspicam_node camerav2_320x240.launch enable_raw:=true
   0------------------> x (cols) Image Frame
   |
   |        c    Camera frame
   |         o---> x
   |         |
   |         V y
   |
   V y (rows)
SUBSCRIBES TO:
    /raspicam_node/image: Source image topic
    
PUBLISHES TO:
    /blob/image_blob : image with detected blob and search window
    /blob/image_mask : masking    
    /blob/point_blob : blob position in adimensional values wrt. camera frame
"""


import sys
import rospy
import cv2
import time

from std_msgs.msg           import String
from sensor_msgs.msg        import Image
from geometry_msgs.msg      import Point
from cv_bridge              import CvBridge, CvBridgeError
from blob_detector  import blob_detect, draw_keypoints, draw_window, draw_frame, blur_outside, get_blob_relative_position

from mira_sensors import MiraSensors

class BlobDetector:

    def __init__(self, thr_min, thr_max, blur=15, blob_params=None, detection_window=None, refresh_rate=10.0):
        
        self.mira_sensors_obj = MiraSensors()
        self.bridge_object = CvBridge()

        self.rate = rospy.Rate(refresh_rate)

        self.set_threshold(thr_min, thr_max)
        self.set_blur(blur)
        self.set_blob_params(blob_params)
        self.detection_window = detection_window
        
        self._t0 = time.time()
        
        self.blob_point = Point()
    
        print (">> Publishing image to topic image_blob")
        self.image_pub = rospy.Publisher("/blob/image_blob",Image,queue_size=1)
        self.mask_pub = rospy.Publisher("/blob/image_mask",Image,queue_size=1)
        print (">> Publishing position to topic point_blob")
        self.blob_pub  = rospy.Publisher("/blob/point_blob",Point,queue_size=1)

        
    def set_threshold(self, thr_min, thr_max):
        self._threshold = [thr_min, thr_max]
        
    def set_blur(self, blur):
        self._blur = blur
      
    def set_blob_params(self, blob_params):
        self._blob_params = blob_params
        
    def loop(self):
        """
        We loop with a certain rate to et a process the image data form Mira
        to find the blob
        """

        while not rospy.is_shutdown():
            
            self.search_blob()
            self.rate.sleep()
        
        cv2.destroyAllWindows()
        
        
    def search_blob(self):
        #--- Assuming image is 320x240
        cv_image = self.mira_sensors_obj.get_image()

        (rows,cols,channels) = cv_image.shape

        if cols > 60 and rows > 60 :
            #--- Detect blobs
            keypoints, mask   = blob_detect(cv_image, self._threshold[0], self._threshold[1], self._blur,
                                            blob_params=self._blob_params, search_window=self.detection_window,
                                            imshow=False )
            print(keypoints)
            #--- Draw search window and blobs
            cv_image    = blur_outside(cv_image, 10, self.detection_window)

            cv_image    = draw_window(cv_image, self.detection_window, line=1)
            cv_image    = draw_frame(cv_image)
            
            cv_image    = draw_keypoints(cv_image, keypoints) 
            
            try:
                self.image_pub.publish(self.bridge_object.cv2_to_imgmsg(cv_image, "bgr8"))
                self.mask_pub.publish(self.bridge_object.cv2_to_imgmsg(mask, "8UC1"))
            except CvBridgeError as e:
                print(e)            

            for i, keyPoint in enumerate(keypoints):
                #--- Here you can implement some tracking algorithm to filter multiple detections
                #--- We are simply getting the first result
                x = keyPoint.pt[0]
                y = keyPoint.pt[1]
                s = keyPoint.size
                print ("kp %d: s = %3d   x = %3d  y= %3d"%(i, s, x, y))
                
                #--- Find x and y position in camera adimensional frame
                x, y = get_blob_relative_position(cv_image, keyPoint)
                
                self.blob_point.x = x
                self.blob_point.y = y
                self.blob_pub.publish(self.blob_point) 
                break
                    
            fps = 1.0/(time.time()-self._t0)
            self._t0 = time.time()
            
def main():

    rospy.init_node('ros_blob_detector', anonymous=True, log_level = rospy.DEBUG)

    # HSV
    ball_colour_min = (0,219,0)
    ball_colour_max = (0,255,255) 
    
    blur     = 5
    
    #--- detection window respect to camera frame in [x_min, y_min, x_max, y_max] adimensional (0 to 1)
    x_min   = 0.1
    x_max   = 0.9
    y_min   = 0.1
    y_max   = 0.8
    
    detection_window = [x_min, y_min, x_max, y_max]
    
    params = cv2.SimpleBlobDetector_Params()
         
    # Change thresholds
    params.minThreshold = 0
    params.maxThreshold = 100
     
    # Filter by Area.
    params.filterByArea = False
    params.minArea = 20
    params.maxArea = 20000
     
    # Filter by Circularity
    params.filterByCircularity = False
    params.minCircularity = 0.1
     
    # Filter by Convexity
    params.filterByConvexity = False
    params.minConvexity = 0.2
     
    # Filter by Inertia
    params.filterByInertia = False
    params.minInertiaRatio = 0.7   

    blob_detector_obj = BlobDetector(ball_colour_min, ball_colour_max, blur, params, detection_window)
    blob_detector_obj.loop()

    rospy.logwarn("Shutting down")
    
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main()